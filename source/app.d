import std.file;
import std.json;
import std.path;

import gtk.MainWindow;
import gtk.Label;
import gtk.Main;

import dide.project.project;
import dide.project_window;
import dide.utils;

/**
 Dide application model.
 */
struct Dide {
  Project[] projects; /// Array of all projects opened in the last 6 months.
}

/**
 Initialises (if necessary) the Dide configuration, generates the "snippets" project if needed too, etc.
 */
void loadOrInitDideModel(ref Dide dide) {
    string configDir = expandTilde("~/.config/dide");
    string projectsConfigDir = configDir ~ "/projects";
    // Make sure DIDE's config directory exists
    mkdirRecurse(projectsConfigDir);
    
    // Load DIDE config

    string jsonFileName = configDir ~ "/dide.json";
    if ( !exists(jsonFileName) ) {
        string dideJsonBody = `{
            "projects": [
                { "name": "snippets", "active": true, "path": "~/.local/dide/snippets"},
                { "name": "foo", "active": false, "path": "/tmp/foo"}
            ]
        }`;
        write(jsonFileName, dideJsonBody);
    }
    JSONValue dideJson = loadJSON(jsonFileName);

    // Load snippets project
    Project snippetsProject = loadSnippetsProject();
    dide.projects = [snippetsProject];
}

/**
 * Handles loading of the file given at command line.
 * Basically, when user runs "dide path/to/file.d", the IDE will start
 * and the startup code will run this function to handle the loading.
 */
void cliLoadFile(string argPath, Dide argDide, ProjectWindow argPW) {
    import std.path: buildNormalizedPath, absolutePath, expandTilde;
    import std.stdio: writeln;
    import std.algorithm: startsWith;

    string filePath = absolutePath(buildNormalizedPath(argPath));
    auto project = argDide.projects[0];
    debug writeln(filePath);
    if (project.contains(filePath)) {
        argPW.pm.maybeOpen(filePath);
    } else {
        debug writeln(filePath, " should be loaded...");
        int addFileResult = project.addFile(filePath, true);
        if (addFileResult == 0) {
            // File has been added to the project successfully, let's create page for it.
            argPW.pm.createFilePage(filePath);
        }
        if (addFileResult == 2) {
            // File is already in the project, but maybe not open by editor
            debug writeln(filePath, " is already in the project.");
            argPW.pm.maybeOpen(filePath);
        }
    }
} // cliLoadFile() function

void main(string[] args) {
    Dide dide;
    loadOrInitDideModel(dide);

    // Initializes gtk runtime
    Main.init(args);

    // Create project window and show it
    ProjectWindow pw = new ProjectWindow(dide.projects[0]);
    pw.showAll();

    if (args.length > 1) {
        cliLoadFile(args[1], dide, pw);
    } // if

    // Starts message loop and displays MainWindow
    Main.run();
} // main() function
