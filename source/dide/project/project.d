module dide.project.project;

import std.json;
import std.file;
import std.path;
import std.conv;
import std.stdio: writeln;
import std.algorithm.mutation: stripLeft, remove;
import std.algorithm.searching: startsWith, canFind;
import std.range: repeat;
import std.array;
import std.string: endsWith;

import dide.config;
import dide.toolchain;
import dide.utils;
import dide.project.project_data_file;

/**
 Supported target types
 */
enum TargetType {
  Source,
  Object,
  SharedObject,
  StaticLibrary,
  Executable,
  Text /// basically anything else that is editable
}

/**
 Represents each particular target of the project.
 */
struct Target {
  TargetType targetType; /// Type of the target.
  /**
   Filesystem path of the target file. It may be either relative or absolute.
   Non-source targets all should have relative paths. Paths of source targets, however,
   depend on the project type. Snippets projects will have source targets with absolute paths,
   whila all other kinds of projects will have them relative to the project directory.
   */
  string path; 
  Target[] sourceTargets; /// An array of target dependencies. Non-auto-generated source files have null here.
  Target[] destTargets; /// An array of destination targets.
  
  /** Is this target part of the project, or is this just some temporary file that is currently edited? */
  bool projectTarget = false;
}

/**
 Various project types.
 */
enum ProjectType {
  Snippets, /// Snippets kind of project (one-file per executable).
  Dide, /// Entirely DIDE managed project (with ability to generage GNU Make file).
  Dub /// Dub-based D project.
}

/**
 Project model.
 */
final class Project { 

  /**
   Most commonly used constructor for Project objects.
   */
  this(string argName, ProjectType argProjectType, string argProjectPath) {
    name = argName;
    projectType = argProjectType;
    projectPath = argProjectPath;

    _srcDir = argProjectPath ~ "/src";
    _libDir = argProjectPath ~ "/lib";
    _exeDir = argProjectPath ~ "/bin";
    _objDir = argProjectPath ~ "/obj";

    // create the "default" Toolchain (system-dmd)
    _toolchain = new Toolchain("default");
    _userConfig = loadUserConfig(name);
  }

  /** 
   Use this method to check whether source file "fileName" is already in the project.
   */
  bool contains(string fileName) {
    foreach (ref target; sourceTargets) {
      if (fileName == target.path) {
        return true;
      }
    }
    return false;
  }

  /**
   Returns:
   0 - everything fine, file has been added to the project.
   1 - something went wrong
   2 - file is already in project
   */
  int addFile(string fileName, bool openFile = false, bool projectTarget = false) {
    int result = 1;
    if (fileName.endsWith(".d")) {
      // is file already in the project?
      if (contains(fileName)) {
        if (openFile && !this.openFiles.canFind(fileName)) {
          openFiles ~= fileName;
        }
        return 2;
      }

      // Source target
      Target srcTarget = Target(TargetType.Source, fileName, null, new Target[1]);
      srcTarget.projectTarget = projectTarget;
      _sourceTargets ~= srcTarget;

      // Object target
      string otmp = fileName.stripLeft('/').replace("/", "_").replace(".d", ".o");
      string newFileName = projectPath ~ "/obj/" ~ otmp;
      // string relPath = relativePath(newFileName, project.projectPath);
      Target objTarget = Target(TargetType.Object, newFileName, [srcTarget], new Target[1]);
      objTarget.projectTarget = projectTarget;
      srcTarget.destTargets[0] = objTarget;
      _activeSourceTarget = srcTarget;

      // Executable target
      string etmp = fileName.stripLeft('/').replace("/", "_").replace(".d", "");
      newFileName = projectPath ~ "/bin/" ~ etmp;
      Target exeTarget = Target(TargetType.Executable, newFileName, [objTarget]);
      exeTarget.projectTarget = projectTarget;
      objTarget.destTargets[0] = exeTarget;
      _topLevelTargets ~= exeTarget;

      if (openFile) {
        // File should be open in the IDE
        _pageToTarget[_openFiles.length] = srcTarget;
        _openFiles ~= fileName;
      }

      result = 0; // All good, we can return 0, meaning we have successfully added the file to the project.
    } else {
      // File is not a D source file. We just add it to the list of open files.
      if (openFile) {
        _openFiles ~= fileName;
      }
      result = 0;
    }

    return result;
  } // addFile() method

  /**
   Associates parge number (in the files notebook) with particular source target.
   It is essential to call this function immediately after we add new page to the files notebook.
   */
  void setPageNumber(int pageNumber) {
    _pageToTarget[pageNumber] = _activeSourceTarget;
  }

  ref Target getTargetByPageNumber(int pageNumber) {
    return _pageToTarget[pageNumber];
  }

  /** 
   * Make a target with fileName as absolute file name path be the active target.
   */
  void setActiveSourceTarget(string fileName) {
    foreach (ref Target target; sourceTargets) {
      if (target.path == fileName) {
        _activeSourceTarget = target;
        return;
      }
    }
  }

  void closeFile(int argPageNumber) {
    debug writeln("Closing " ~ to!string(argPageNumber));
    _openFiles = remove(_openFiles, argPageNumber);
  }

  Target* targetByPath(string argPath) {
    foreach (i, target; sourceTargets) {
      if (target.path == argPath) {
        return &sourceTargets[i];
      }
    }
    return null;
  }

  /** 
   * Source file that is in page with number argPageNumber
   * becomes part of the project. Behind the scenes this method sets the source target's
   * projectTarget property to true.
     Returns
     -------
     true if file has been added to the project.
   */
  bool addFileToProject(int argPageNumber) {
    bool result = false;
    auto tgt = getTargetByPageNumber(argPageNumber);
    debug writeln(tgt.path);
    debug writeln(_sourceTargets.length);
    Target* targetPtr = targetByPath(tgt.path);
    if (targetPtr) {
      if (targetPtr.projectTarget == false) {
        result = true;
      }
      targetPtr.projectTarget = true;
    }
    return result;
  } // addFileToProject() method

  /** 
   * Use this function at the beginning to populate Project with files we know are part of the projects.
   * Parameters
   * ----------
   * argFilePaths : string[]
   *   An array of files that are part of the project. The array may contain files that are not source files!
   */
  void loadProjectFiles(string[] argFilePaths) {
    foreach (filePath; argFilePaths) {
      addFile(filePath, false, true);
    }
  } // loadProjectFiles() method

  string shortPath(ref Target argTarget) {
    string result = argTarget.path;
    string home = expandTilde("~/"); // "~/" --> "/home/dejan/"
    if (argTarget.path.startsWith(projectPath)) {
      result = "*" ~ argTarget.path[projectPath.length..$];
    } else if (argTarget.path.startsWith(home)) {
      result = "~/" ~ argTarget.path[home.length..$];
    }
    return result;
  }

  /** 
   * Useful tiny function - whenever you need to check whether argTarget is open
   * in DIDE editor.
   */
  bool isOpen(ref Target argTarget) {
    foreach (string path; openFiles) {
      if (path == argTarget.path) {
        return true;
      }
    }
    return false;
  } // isOpen() method

  bool removeFileByPath(string argPath) {
    size_t idx = -1;
    auto sizeBefore = sourceTargets.length;
    foreach (tgt_idx, tgt; sourceTargets) {
      writeln(tgt.path);
      if (tgt.path == argPath) {
        idx = tgt_idx;
        writeln(idx);
        break;
      }
    }
    _sourceTargets = _sourceTargets.remove(idx);
    if (sizeBefore > _sourceTargets.length) { 
      return true; 
    } else { 
      return false; 
    }
  } // removeFileByPath() method

  /+-------------------------------------- PUBLIC PROPERTIES ------------------------------------------+/
  @property string name() { return _name; }
  @property void name(string argName) { _name = argName; }
  @property ProjectType projectType() { return _projectType; }
  @property void projectType(ProjectType argProjectType) { _projectType = argProjectType; }
  @property string projectPath() { return _projectPath; }
  @property void projectPath(string argPath) { _projectPath = argPath; }
  @property Target[] topLevelTargets() { return _topLevelTargets; }
  @property void topLevelTargets(Target[] argTargets) { _topLevelTargets = argTargets; }
  @property Toolchain toolchain() { return _toolchain; }
  @property void toolchain(Toolchain argToolchain) { _toolchain = argToolchain; }
  @property Target[] sourceTargets() { return _sourceTargets; }
  @property void sourceTargets(Target[] argTargets) { _sourceTargets = argTargets; }
  /// Use this property to get the last added source target
  @property Target activeSourceTarget() { return _activeSourceTarget; }
  @property ProjectConfig projectConfig() { return _projectConfig; }
  @property void projectConfig(ref ProjectConfig argProjectConfig) { _projectConfig = argProjectConfig; }
  @property UserConfig userConfig() { return _userConfig; }
  @property void userConfig(ref UserConfig argUserConfig) { _userConfig = argUserConfig; }
  @property ref string[] openFiles() { return _openFiles; }
  @property void openFiles(string[] argOpenFiles) { _openFiles = argOpenFiles; }
  @property string srcDir() { return _srcDir; }
  @property void srcDir(string argName) { _srcDir = argName; }

  /+-------------------------------------- PRIVATE STUFF ----------------------------------------------+/
  private:

  string _name; /// Name of the project.
  ProjectType _projectType; /// Type of the project
  string _projectPath; /// Absolute path to the project's root directory.
  Target[] _topLevelTargets; /// Top-level targets of the project
  Toolchain _toolchain; /// Toolchain used by this project
  Target[] _sourceTargets; /// Array containing all project source files
  Target _activeSourceTarget; /// Source target that is currently active (editing)
  Target[ulong] _pageToTarget; /// Maps page to particular (source) target
  ProjectConfig _projectConfig; /// Project-level configuration
  UserConfig _userConfig; /// User-level configuration
  string[] _openFiles;  /// Files that are open in the project window
  string _srcDir;  /// Directory where project source files are
  string _libDir;  /// Directory where project libraries (both library targets, and external libraries)
  string _exeDir;  /// Directory where project executables are (both executable targets and executables created manually)
  string _objDir;  /// Directory where project object files are generated
}

/**
 Loads the "snippets" project
 */
Project loadSnippetsProject() { 
  string dideProjectDir = expandTilde("~/.local/dide");
  string snippetsProjectDir = dideProjectDir ~ "/snippets";
  debug writeln(snippetsProjectDir);
  if (!exists(snippetsProjectDir) ) {
      // Make sure DIDE/snippets directory exists
      mkdirRecurse(snippetsProjectDir);
      
      mkdir(snippetsProjectDir ~ "/obj"); // where all object file targets go
      mkdir(snippetsProjectDir ~ "/bin"); // where all executable targets go
      mkdir(snippetsProjectDir ~ "/lib"); // where all library targets go (both shared and static)
      mkdir(snippetsProjectDir ~ "/src"); // new source files go here
  }

  JSONValue snippetsJson;
  string jsonFileName = snippetsProjectDir ~ "/dide.json";
  if ( !exists(jsonFileName) ) {
      string snippetsJsonBody = `{
          "name": "snippets",
    "type": 0
      }`;
      write(jsonFileName, snippetsJsonBody);
  }
  
  Project project = new Project("snippets", ProjectType.Snippets, snippetsProjectDir);
  ProjectDataFile projdf = new ProjectDataFile(project);
  projdf.load();

  string userConfigJSONFile = expandTilde("~/.config/dide/projects/snippets.json");
  if (exists(userConfigJSONFile)) {
    JSONValue jsonUserConfig = parseJSON(std.file.readText(userConfigJSONFile), JSONOptions.doNotEscapeSlashes);
    debug writeln("-------------------");
    foreach (jsonValue; jsonUserConfig["open_files"].array) {
      int res = project.addFile(jsonValue.str, true);
    }
  } else {
    // Do nothing if the snippets.json is not there
  }
  return project;
}

/**
 Nicely prints (to stdout) the project hierarchy. All targets will be printed in a tree-like structure.
 
 NOTE: These two print() functions are the only place in DIDE where we output to STDOUT even when built in 
       release mode.
 */
void print(ref Target target, int level = 0) {
  string targetType = "";
  switch (target.targetType) {
    case TargetType.Source: 
      targetType = "S";
      break;
    case TargetType.Object: 
      targetType = "O";
      break;
    case TargetType.Executable: 
      targetType = "E";
      break;
    default:
      targetType = "U";
  }

  writeln(targetType, " ".repeat(level).join(), target.path);
  writeln("  ", " ".repeat(level).join(), target.destTargets.length);
  if (target.sourceTargets.length > 0) {
    foreach (ref tgt; target.sourceTargets) {
      print(tgt, level + 2);
    }
  }
}

/**
 Output the project (target hierarchy) to the console. 

 NOTE: These two print() functions are the only place in DIDE where we output to STDOUT even when built in 
       release mode.
 */
void print(Project project) {
  writeln(project.name);
  writeln('-'.repeat(80));
  if (project.topLevelTargets.length > 0) {
    foreach (ref tgt; project.topLevelTargets) {
      print(tgt);
    }
  } else {
    writeln("No targets in this project");
  }
}
