module dide.project_window;

// D
import std.array: replace;
import std.conv;
import std.file: write, getcwd;
import std.stdio: writeln;
import std.string: endsWith;
import std.path: baseName;

// GTK Core
import gtk.AccelGroup;
import gtk.Box;
import gtk.HBox;
import gtk.HPaned;
import gtk.Label;
import gtk.ListBox;
import gtk.ListBoxRow;
import gtk.Main;
import gtk.MainWindow;
import gtk.Menu;
import gtk.MenuBar;
import gtk.MenuItem;
import gtk.Notebook;
import gtk.ScrolledWindow;
import gtk.Separator;
import gtk.SeparatorMenuItem;
import gtk.SeparatorToolItem;
import gtk.StockItem;
import gtk.TextView;
import gtk.TextBuffer;
import gtk.TextTagTable;
import gtk.TextIter;
import gtk.Toolbar;
import gtk.ToolButton;
import gtk.ToolItemGroup;
import gtk.VPaned;
import gtk.Widget;
import gtk.Window;
import gtk.FileChooserDialog;
// GTK/SourceView
import gsv.SourceBuffer;
import gsv.SourceBuffer;
import gsv.SourceLanguage;
import gsv.SourceLanguageManager;
import gsv.SourceStyleSchemeManager;
import gsv.SourceView;
// GTK/VTE
import vte.Terminal;
// GDK
import gdk.Keysyms;

// DIDE
import dide.project.project;
import dide.project_manager;
import dide.config;
import dide.utils;

/**
 Project Window.
 */
class ProjectWindow: MainWindow {
  Box _mainBox; /// Main container
  Notebook projectNb; /// Project notebook
  Notebook filesNb; /// Files notebook (where we edit files)
  Notebook controlNb; /// Control notebook
  HPaned mainPaned; /// Main paned
  VPaned filesAndControlPaned; /// Paned containing files and control notebooks
  ProjectManager pm; /// ProjectManager object associated with the window
  AccelGroup accelGroup; /// AccelGroup object
  Project project; /// Project model
  ListBox srcListBox; /// ListBox with project source targets
  ListBox tltListBox; /// ListBox with project executable/object targets

  /**
   Most of the time we will use this constructor, which takes a Project (model) reference.
   */
  this(Project argProject) {
    super("DIDE: " ~ argProject.name);
    project = argProject;
    pm = ProjectManager(argProject, this, null);
    accelGroup = new AccelGroup();
    addAccelGroup(accelGroup);
    debug writeln("loaded config: ", project.userConfig);
    // If previous project window was maximised, maximise it, if not check the other saved dimensions
    // and try to restore the UI
    if (project.userConfig.ui_window_maximised) {
      maximize();
    } else {
      int w = (project.userConfig.ui_window_w == -1) ? 1024 : project.userConfig.ui_window_w;
      int h = (project.userConfig.ui_window_h == -1) ? 768 : project.userConfig.ui_window_h;
      setDefaultSize(w, h);
      int x = (project.userConfig.ui_window_x == -1) ? 0 : project.userConfig.ui_window_x;
      int y = (project.userConfig.ui_window_y == -1) ? 0 : project.userConfig.ui_window_y;
      move(x, y);
    }
    
    _mainBox = new Box(Orientation.VERTICAL, 0);
    initMenuBar();
    initToolbar();
    initMainArea();
    pm.filesNotebook = filesNb;
    initStatusBar();
    add(_mainBox);
    // set icon for the project window
    setDefaultIconName("document-page-setup");

    // Open all files that were previously open
    foreach (fileName; project.openFiles) {
      pm.createFilePage(fileName);
    }
    if (project.openFiles.length == 0) {
      filesNb.appendPage(getSourceView(), new Label("Unknown"));
    }
    // Make the current page be the one that was active last time we ran DIDE
    int lastPageId = project.userConfig.filesNbActivePageId;
    if (filesNb.getNPages > lastPageId) {
      filesNb.setCurrentPage(lastPageId);
    }
    filesNb.popupEnable();

    addOnDestroy((Widget w) => pm.handleQuit(true));
    
    addOnConfigure(&_handleConfigureEvent);
  }

  /**
   Writes to the projects userConfig struct all the important details about the project window so it later can be 
   saved to a file. Called from ProjectManager to update project object before saving.

   skipXY is a flag used to tell the function not to update X and Y positions. This is needed so that
   when onDestroy() triggers it X and Y do not become zeroes.
   */
  void updateUserConfig(bool skipXY=false) {
    debug writeln("updateUserConfig(", skipXY, ")");
    UserConfig userConfig = project.userConfig;
    userConfig.ui_files_h = filesAndControlPaned.getPosition;
    userConfig.ui_window_maximised = isMaximized;
    userConfig.ui_project_w = mainPaned.getPosition;
    
    if (!skipXY) {
      getPosition(userConfig.ui_window_x, userConfig.ui_window_y);
    }
    getSize(userConfig.ui_window_w, userConfig.ui_window_h);
    
    userConfig.projectNbVisible = projectNb.isVisible();
    userConfig.projectNbActivePageId = cast(ubyte)(projectNb.getCurrentPage);
    userConfig.controlNbVisible = controlNb.isVisible();
    userConfig.controlNbActivePageId = cast(ubyte)(controlNb.getCurrentPage);
    userConfig.filesNbActivePageId = cast(ubyte)(this.filesNb.getCurrentPage);
    project.userConfig = userConfig;
  }

  private:
  
  bool _handleConfigureEvent(GdkEventConfigure* event, Widget widget) {
    // debug writeln("_handleConfigureEvent()");
    UserConfig uc = project.userConfig;
    if (event.type is EventType.CONFIGURE) {
      uc.ui_window_x = event.x;
      uc.ui_window_y = event.y;
    }
    project.userConfig = uc;
    return false;
  }
  
  void handleNewFile(MenuItem mi) {
    int pageNumber = filesNb.appendPage(getSourceView(), new Label("Unknown"));
    filesNb.showAll();
    // Has to be called after showAll() , otherwise it will not switch to the new page
    filesNb.setCurrentPage(pageNumber);
  }

  void handleOpenFile(MenuItem mi) {
    pm.openFile();
  }

  void handleSaveFile(MenuItem mi) {
    const int pageNumber = filesNb.getCurrentPage();
    auto childWidget = filesNb.getNthPage(pageNumber);
    string label = filesNb.getTabLabelText(childWidget);
    
    bool shouldSaveContent = true;
    if (label == "Unknown") {
      shouldSaveContent = false;
      // We are on a page where we have a content that hasn't been saved yet.
      int response;
      FileChooserAction action = FileChooserAction.SAVE;
      
      //string filename = getcwd() ~ "/Unknown.d";
      string filename = project.srcDir ~ "/Unknown.d";

      FileChooserDialog dialog = new FileChooserDialog("Save a File", this, action, null, null);
      dialog.setCurrentName(filename);
      response = dialog.run();

      if (response == ResponseType.OK) {
        filename = dialog.getFilename();
        debug writeln(filename);
        if (filename.endsWith(".d")) {
          project.addFile(filename);
          project.setPageNumber(pageNumber);
        }
        shouldSaveContent = true;
        filesNb.setTabLabelText(childWidget, baseName(filename));
      } else {
        debug writeln("cancelled.");
      }
      dialog.destroy();
    }

    if (shouldSaveContent) {
      ScrolledWindow scrlw = cast(ScrolledWindow)(childWidget);
      SourceView sv = cast(SourceView)(scrlw.getChild());
      SourceBuffer sb = sv.getBuffer();
      if (sb is null) {
        debug writeln("Could not get SV!");
      } else {
        Target tgt  = project.getTargetByPageNumber(pageNumber);
        debug writeln(tgt.targetType, "/", tgt.path);
        write(tgt.path, sb.getText());
      }
    }
  } // handleSaveFile() method

  void initMenuBar() {
    MenuBar menuBar = new MenuBar();

    // FILE
    MenuItem fileMenuItem = new MenuItem("_File");
    Menu fileMenu = new Menu();
    fileMenuItem.setSubmenu = fileMenu;
    
    // File/New
    MenuItem newMenuItem = new MenuItem(&handleNewFile, "_New", "activate", true, accelGroup, 'n', 
    ModifierType.CONTROL_MASK, AccelFlags.VISIBLE);
    fileMenu.add(newMenuItem);

    // File/Open...
    MenuItem openMenuItem = new MenuItem(&handleOpenFile, "_Open...", "activate", true, accelGroup, 'o', 
    ModifierType.CONTROL_MASK, AccelFlags.VISIBLE);
    // openMenuItem.addOnActivate(&handleOpenFile);
    fileMenu.add(openMenuItem);
    
    // File/-----------
    fileMenu.add(new SeparatorMenuItem);

    // File/Save
    MenuItem saveMenuItem = new MenuItem(&handleSaveFile, "_Save", "activate", true, accelGroup, 's', 
    ModifierType.CONTROL_MASK, AccelFlags.VISIBLE);
    fileMenu.add(saveMenuItem);

    MenuItem closeMenuItem = new MenuItem((MenuItem mi) => pm.handleClose, "_Close", "activate", true, accelGroup, 'w', 
                                         ModifierType.CONTROL_MASK | ModifierType.SHIFT_MASK, AccelFlags.VISIBLE);
    fileMenu.add(closeMenuItem);

    MenuItem quitMenuItem = new MenuItem((MenuItem mi) => pm.handleQuit, "_Quit", "activate", true, accelGroup, 'q', 
                                         ModifierType.CONTROL_MASK, AccelFlags.VISIBLE);
    fileMenu.add(quitMenuItem);

    // finally, add the file menu item to the menu bar
    menuBar.add(fileMenuItem);

    // EDIT
    MenuItem editMenuItem = new MenuItem("_Edit");
    Menu editMenu = new Menu();
    editMenuItem.setSubmenu(editMenu);
    MenuItem undoMenuItem = new MenuItem("_Undo");
    editMenu.add(undoMenuItem);
    MenuItem redoMenuItem = new MenuItem("_Redo");
    editMenu.add(redoMenuItem);
    editMenu.add(new SeparatorMenuItem());
    MenuItem copyMenuItem = new MenuItem("_Copy");
    editMenu.add(copyMenuItem);
    MenuItem pasteMenuItem = new MenuItem("_Paste");
    editMenu.add(pasteMenuItem);
    MenuItem cutMenuItem = new MenuItem("Cu_t");
    editMenu.add(cutMenuItem);
    menuBar.add(editMenuItem);

    // Project
    MenuItem projectMenuItem = new MenuItem("_Project");
    Menu projectMenu = new Menu();
    projectMenuItem.setSubmenu(projectMenu);

    MenuItem compileFileItem = new MenuItem("_Compile file", (MenuItem mi) => pm.compileFile(), "activate");
    // NOTE: Some desktop managers reserve CTRL-Fxx - https://forum.xfce.org/viewtopic.php?id=16032
    compileFileItem.addAccelerator("activate", accelGroup, GdkKeysyms.GDK_F6, ModifierType.CONTROL_MASK, 
                                AccelFlags.VISIBLE);
    projectMenu.add(compileFileItem);

    MenuItem runFileItem = new MenuItem("_Run file", (MenuItem mi) => pm.runFile(), "activate");
    runFileItem.addAccelerator("activate", accelGroup, GdkKeysyms.GDK_F6, ModifierType.SHIFT_MASK, 
                                AccelFlags.VISIBLE);
    projectMenu.add(runFileItem);
    
    projectMenu.add(new SeparatorMenuItem);

    MenuItem addFileMenuItem = new MenuItem(
      (MenuItem mi) => pm.projectAddFileHandler(), "Add file", "activate", true, accelGroup, '=', 
      ModifierType.CONTROL_MASK, AccelFlags.VISIBLE);
    // MenuItem addFileMenuItem = new MenuItem("Add file");
    // addFileMenuItem.addOnActivate((MenuItem mi) => pm.projectAddFileHandler());
    projectMenu.add(addFileMenuItem);

    MenuItem removeFileMenuItem = new MenuItem(
      (MenuItem mi) => pm.projectRemoveFileHandler(), "Remove file", "activate", true, accelGroup, '-', 
      ModifierType.CONTROL_MASK,AccelFlags.VISIBLE);
    projectMenu.add(removeFileMenuItem);

    menuBar.add(projectMenuItem);

    // WIP
    version (wip) {
      // We enable this piece of code only if DIDE is compiled with "wip" flag
      MenuItem wipMenuItem = new MenuItem("WIP");
      Menu wipMenu = new Menu();
      wipMenuItem.setSubmenu(wipMenu);

      MenuItem printMenuItem = new MenuItem("_Print");
      printMenuItem.addOnActivate((MenuItem mi) => print(project));
      wipMenu.add(printMenuItem);

      MenuItem displayMenuItem = new MenuItem("_Print number of monitors");
      displayMenuItem.addOnActivate((MenuItem mi) {
        import gdk.Display;
        int numMonitors = Display.getDefault.getNMonitors();
        debug writeln("Number of monitors: ", numMonitors);
        updateUserConfig();
        debug writeln(pm.project.userConfig);
      });
      wipMenu.add(displayMenuItem);

      MenuItem tcDetectMenuItem = new MenuItem((MenuItem mi) => pm.handleDetectTC(), 
        "_Toolchain detect");
      wipMenu.add(tcDetectMenuItem);


      // Finally, add the WIP menu
      menuBar.add(wipMenuItem);
    }

    // Help ----------------------------------------------------------------------------------------
    MenuItem helpMenuItem = new MenuItem("_Help");
    Menu helpMenu  = new Menu();
    helpMenuItem.setSubmenu(helpMenu);
    MenuItem aboutMenuItem = new MenuItem("_About");
    aboutMenuItem.addOnActivate((MenuItem mi) => pm.handleAbout(mi));
    helpMenu.add(aboutMenuItem);
    menuBar.add(helpMenuItem);

    _mainBox.packStart(menuBar, false, false, 0);
  }

  void initToolbar() {
    Toolbar toolbar = new Toolbar();
    ToolButton undoButton = new ToolButton(StockID.UNDO);
    toolbar.add(undoButton);
    ToolButton redoButton = new ToolButton(StockID.REDO);
    toolbar.add(redoButton);
    SeparatorToolItem separator = new SeparatorToolItem();
    toolbar.add(separator);
    ToolButton copyButton = new ToolButton(StockID.COPY);
    toolbar.add(copyButton);
    ToolButton pasteButton = new ToolButton(StockID.PASTE);
    toolbar.add(pasteButton);
    ToolButton cutButton = new ToolButton(StockID.CUT);
    toolbar.add(cutButton);

    _mainBox.packStart(toolbar, false, false, 0);
  }

  Widget getSourceView() {
    SourceView sourceView = new SourceView();
    sourceView.setShowLineNumbers(true);

    sourceView.setInsertSpacesInsteadOfTabs(true);
    sourceView.setTabWidth(4);
    sourceView.setHighlightCurrentLine(true);

    SourceBuffer sb = sourceView.getBuffer();

    auto manager = new SourceStyleSchemeManager();
    auto sids = manager.getSchemeIds();
    debug writeln(sids);
    auto scheme = manager.getScheme("oblivion");
    sb.setStyleScheme(scheme);

    string code = `import std.stdio;
    
    void main() {
      writeln("hello");
    }`.replace("        ", "");
    sb.setText(code);

    ScrolledWindow scWindow = new ScrolledWindow();
    scWindow.add(sourceView);


    SourceLanguageManager slm = new SourceLanguageManager();
    SourceLanguage dLang = slm.getLanguage("d");

    if (dLang !is null) {
        // writefln("Setting language to D");
        sb.setLanguage(dLang);
        sb.setHighlightSyntax(true);
    }

    sourceView.modifyFont("Terminus", 10);
    sourceView.setRightMarginPosition(110);
    sourceView.setShowRightMargin(true);
    sourceView.setAutoIndent(true);

    return scWindow;
  }

  /**
   Must be called after the controlNb Notebook object is created. 
   */
  void initBuildPage() {
    TextTagTable tgt = new TextTagTable();
    TextBuffer btvb = new TextBuffer(tgt);

    // Create some tags. We will keep it in this method for the time being...
    // TODO: make a dedicated method for it.
    btvb.createTag("bold", "weight", cast(int)PangoWeight.BOLD);
		btvb.createTag("monospace", "family", "monospace");
		btvb.createTag("fg_blue", "foreground", "blue");
    btvb.createTag("fg_green", "foreground", "green");
    btvb.createTag("fg_orange", "foreground", "orange");
		btvb.createTag("bg_red", "background", "red");
    

    TextView buildTextView = new TextView(btvb);
    // We wrap the text view in a ScrolledWindow, so that we can add it to the Notebook
    ScrolledWindow sw = new ScrolledWindow(PolicyType.AUTOMATIC, PolicyType.AUTOMATIC);
    sw.add(buildTextView);
    
    TextIter iter = new TextIter();
		// TextIter start = new TextIter();
		// TextIter end = new TextIter();
    btvb.getIterAtOffset(iter, 0);
    btvb.insert(iter, "Not implemented yet\n\n");
    btvb.insert(iter, "Not implemented yet\n");
    btvb.insertWithTagsByName(iter, "dmd -g /tmp/file.d -of /tmp/file.o\n", ["bold", "monospace", "fg_blue"]);
    controlNb.appendPage(sw, new Label("Build"));
    /* Project manager needs to have a reference to the TextView so it can
    write to the text buffer when it is building stuff */
    pm.buildTextView = buildTextView;
  }

  void initFilesAndControl() {
    filesAndControlPaned = new VPaned;
    filesNb = new Notebook();
    controlNb = new Notebook();
    // controlNb.appendPage(new Label("Not implemented"), new Label("Build"));
    initBuildPage();
    controlNb.appendPage(new Label("Not implemented"), new Label("Debug"));
    controlNb.appendPage(new Label("Not implemented"), new Label("Run"));
    initTerminalPage();
    filesAndControlPaned.add(filesNb, controlNb);

    // If the position is saved in the user configuration, restore it, if not use 2/3 of the window size
    int w, h;
    getSize(w, h);
    h = (project.userConfig.ui_files_h == -1) ? (2 * h / 3) : project.userConfig.ui_files_h;
    filesAndControlPaned.setPosition(h);
    
    // We have to call showAll() because setCurrentPage() will not work correctly if we do not.
    controlNb.showAll();
    int lastActivePageId = project.userConfig.controlNbActivePageId;
    if (controlNb.getNPages > lastActivePageId) {
      controlNb.setCurrentPage(lastActivePageId);
    }
  }

  void initMainArea() {
    mainPaned = new HPaned();

    projectNb = new Notebook();
    projectNb.setTabPos(PositionType.LEFT);

    // TLT
    tltListBox = new ListBox;
    Label tltFooItem = new Label("bin/foo");
    tltFooItem.setHalign(Align.END);
    tltListBox.add(tltFooItem);
    Label tltBarItem = new Label("bin/bar");
    tltBarItem.setHalign(Align.END);
    tltListBox.add(tltBarItem);
    
    Label tltTabLabel = new Label("TLT");
    tltTabLabel.setAngle(90.0);
    projectNb.appendPage(tltListBox, tltTabLabel);

    // SRC
    srcListBox = new ListBox();
    srcListBox.setActivateOnSingleClick(false);
    srcListBox.addOnRowActivated((ListBoxRow argListBoxRow, ListBox argListBox) {
      int idx = argListBoxRow.getIndex();
      debug writeln(idx);
      Widget child = argListBoxRow.getChild();
      if (child) {
        debug writeln("Loading ", child.getName());
        pm.maybeOpen(child.getName());
      }
    });

    foreach (target; project.sourceTargets) {
      debug writeln("TTT ", target.path);
      if (target.projectTarget) {
        // Issue: dejan/dide#15
        // We have to replace one underscore with two because GTK treats single one as start of mnemonic.
        string caption = project.shortPath(target).replace("_", "__");
        Label tmpLabel = new Label(caption);
        tmpLabel.setName(target.path);
        // We set user-data for every label inside the list-box
        tmpLabel.setData("target", cast(void*)&target);
        
        tmpLabel.setHalign(Align.END);
        srcListBox.add(tmpLabel);
      }
    }
    Label srcTabLabel = new Label("SRC");
    srcTabLabel.setAngle(90.0);
    projectNb.appendPage(srcListBox, srcTabLabel);
    
    // We have to call showAll() because setCurrentPage() will not work correctly if we do not.
    projectNb.showAll();
    int lastActivePageId = project.userConfig.projectNbActivePageId;
    if (projectNb.getNPages > lastActivePageId) {
      projectNb.setCurrentPage(lastActivePageId);
    }

    // Initialise files and control paned objects before we put all that into the main paned
    initFilesAndControl();
    
    // If the position is saved in the user configuration, restore it, if not 1/4 of the window size
    // for the project panel
    int w, h;
    getSize(w, h);
    w = (project.userConfig.ui_project_w == -1) ? (w / 4 ) : project.userConfig.ui_project_w;
    mainPaned.setPosition(w);

    mainPaned.add(projectNb, filesAndControlPaned);
    _mainBox.packStart(mainPaned, true, true, 0);
  }

  void initStatusBar() {
    HBox statusBarBox = new HBox(true, 2);
    auto projPathLabel = new Label(project.projectPath);
    projPathLabel.setHalign(Align.START);
    
    statusBarBox.add(projPathLabel);
    // statusBarBox.add(new Separator(Orientation.VERTICAL));
    
    // Label that goes into the middle of the status bar - not in use for the time being.
    statusBarBox.add(new Label(""));
    // statusBarBox.add(new Separator(Orientation.VERTICAL));
    
    string dtString = dateTimeString();
    auto dtLabel = new Label(dtString);
    dtLabel.setHalign(Align.END);
    statusBarBox.add(dtLabel);
    
    // Let's create a timer that will update the bottom-right label with time every 5 seconds.
    // With lowest priority, as updates of the clock label are not of critical importance.
    import glib.Timeout : Timeout;
    import gtkc.gtktypes : GPriority;
    auto clockTimeout = new Timeout(5000, delegate bool() {
      string dtString = dateTimeString();
      dtLabel.setText(dtString);
      return true;
    }, GPriority.LOW);

    _mainBox.packStart(statusBarBox, false, false, 0);
  }

  void initTerminalPage() {
    Terminal terminal = new Terminal();
    terminal.spawnAsync(PtyFlags.DEFAULT, project.projectPath, ["/usr/bin/env",  "bash"], null, SpawnFlags.DEFAULT, 
                        null, null, null, 3600, null, null, null);
    controlNb.appendPage(terminal, new Label("VTE"));
  }
}
