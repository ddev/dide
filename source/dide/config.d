module dide.config;

import std.json;
import std.path;
import std.stdio: writeln;
import std.file: exists, write;

import dide.utils;

/**
 Project-level config. Stored in VCS typically and shared among developers.
 Should be in line with DFMT.
 */
struct ProjectConfig {
  int code_style_tab_width = 2;
  int code_style_indent_size = 2;
  bool code_style_use_tab = false; // indent_style: tab, space   default: space
}

/**
 User settings for particular project.
 */
struct UserConfig {
  bool ui_window_maximised = false;
  int ui_window_x;
  int ui_window_y;
  int ui_window_w = 800;
  int ui_window_h = 600;
  int ui_project_w = 200;
  int ui_files_h = 200;
  bool projectNbVisible = true; /// Indicates whether project notebook is hiden (false) or not
  ubyte projectNbActivePageId = 0;  /// Which page in the project notebook is active?
  bool controlNbVisible = true;
  ubyte controlNbActivePageId = 0;
  ubyte filesNbActivePageId = 0;
}

void saveUserConfig(UserConfig argUserConfig, string[] argOpenFiles, string argProjectName) {
  JSONValue json = ["foo": "bar"];
  JSONValue ui = ["foo": "bar"];
  ui.object["window_maximised"] = JSONValue(argUserConfig.ui_window_maximised);
  ui.object["files_h"] = JSONValue(argUserConfig.ui_files_h);
  ui.object["project_w"] = JSONValue(argUserConfig.ui_project_w);
  ui.object["window_x"] = JSONValue(argUserConfig.ui_window_x);
  ui.object["window_y"] = JSONValue(argUserConfig.ui_window_y);
  ui.object["window_w"] = JSONValue(argUserConfig.ui_window_w);
  ui.object["window_h"] = JSONValue(argUserConfig.ui_window_h);
  ui.object["project_nb_visible"] = JSONValue(argUserConfig.projectNbVisible);
  ui.object["project_nb_active_page_id"] = JSONValue(argUserConfig.projectNbActivePageId);
  ui.object["control_nb_visible"] = JSONValue(argUserConfig.controlNbVisible);
  ui.object["control_nb_active_page_id"] = JSONValue(argUserConfig.controlNbActivePageId);
  ui.object["files_nb_active_page_id"] = JSONValue(argUserConfig.filesNbActivePageId);
  json.object["ui"] = ui;

  JSONValue[] openFiles;
  foreach (ofile; argOpenFiles) {
    debug writeln(ofile);
    openFiles ~= JSONValue(ofile);
  }
  json.object["open_files"] = openFiles;
  string jsonFileName = expandTilde("~/.config/dide/projects/" ~ argProjectName ~ ".json");
  write(jsonFileName, json.toPrettyString(JSONOptions.doNotEscapeSlashes));
}

UserConfig loadUserConfig(string argProjectName) {
  UserConfig ret;
  string jsonFileName = expandTilde("~/.config/dide/projects/" ~ argProjectName ~ ".json");
  if (exists(jsonFileName)) {
    JSONValue json = loadJSON(jsonFileName);
    JSONValue json_ui = json["ui"];
    ret.ui_window_maximised = json_ui["window_maximised"].boolean;
    ret.ui_window_x = json_ui["window_x"].get!int;
    ret.ui_window_y = json_ui["window_y"].get!int;
    ret.ui_window_w = json_ui["window_w"].get!int;
    ret.ui_window_h = json_ui["window_h"].get!int;
    ret.ui_files_h = json_ui["files_h"].get!int;
    ret.ui_project_w = json_ui["project_w"].get!int;
    
    ret.projectNbVisible = ("project_nb_visible" in json_ui) ? json_ui["project_nb_visible"].get!bool : true;
    ret.projectNbActivePageId = ("project_nb_active_page_id" in json_ui) 
                                ? json_ui["project_nb_active_page_id"].get!ubyte 
                                : 0;
    ret.controlNbVisible = ("control_nb_visible" in json_ui) ? json_ui["control_nb_visible"].get!bool : true;
    ret.controlNbActivePageId = ("control_nb_active_page_id" in json_ui) 
                                ? json_ui["control_nb_active_page_id"].get!ubyte 
                                : 0;
    ret.filesNbActivePageId = ("files_nb_active_page_id" in json_ui) 
                                ? json_ui["files_nb_active_page_id"].get!ubyte 
                                : 0;
  } else {
    debug writeln("JSON file " ~ jsonFileName ~ " not found. Using defaults.");
  }
  return ret;
}
