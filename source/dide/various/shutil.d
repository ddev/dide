module dide.various.shutil;

import std.algorithm.searching;

import std.file: dirEntries, SpanMode;
import std.path: baseName, expandTilde;
import std.process: environment;
import std.range: split;
import std.stdio: writeln;

/**
  This function behaves similarly to the "which" command-line tool on UNIX/Linux - if tool named
  argExeName is found in PATH, it will be returned, otherwise results in null.

  Parameters:
    string argExeName - Name of the tool we are looking for.

  Returns: full path to the executable, or null if not found.
*/
string which(string argExeName) {
  string ret = null;
  auto pathEnv = environment.get("PATH", "/usr/bin:/usr/local/bin:~/bin");

  foreach (path; pathEnv.split(":")) {
    foreach (dirEntry; dirEntries(path, SpanMode.shallow)) {
      string fileName = dirEntry.name;
      debug writeln(fileName);
      if (dirEntry.isFile) {
        if (fileName.baseName == argExeName) {
          return fileName;
        }
      }
    }
  }
  return ret;
} // which() function
