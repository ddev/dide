module dide.dide_accel_group;

import gtk.AccelGroup;

/**
 An implementation of the AccelGroup as a singleton, so we do not have to pass it around all the time.
 This may be temporary solution until we start working on the planned feature to actually have multiple
 project windows, in which case this will probably not work.
 */
class DideAccelGroup: AccelGroup {
  static DideAccelGroup get() {
    if (!_instantiated) {
      synchronized (DideAccelGroup.classinfo) {
        if (!_instance) {
          _instance = new DideAccelGroup();
        }

        _instantiated = true;
      }
    }

    return _instance;
  }

  private:

  this() {
    super();
  }

  // Cache instantiation flag in thread-local bool
  static bool _instantiated;

  // Thread global
  __gshared DideAccelGroup _instance;
} // DideAccelGroup class
