# DIDE

D IDE - A simple integrated development environment (IDE) for D programming. 
Acronym actually means **D**-ejan's **I**-ntegrated **D**-evelopment **E**-nvironment.

**Life of this project continues on CodeBerg - https://codeberg.org/dejan/dide**

At the moment of writing this, DIDE is a personal project, addressing some of the needs I have for the D programming.
However, our needs may be aligned, in which case I encourage you to contribute to the project either by development 
or testing. If you would like to chat directly to me on IRC, please find me on OFTC or Libera.Chat, nick "Dejan".

Official IRC channel is #DIDE on OFTC - irc://irc.oftc.net/#DIDE

Build and run
-------------

Follow these steps to build and run DIDE:

```
cd /tmp
git clone https://gitlab.com/dejan/dide.git
cd dide
dub build
dub run  # or just ./dide
```

There will be no official pre-built binaries until release 1.0.0 (which may take even a year) so if you want to use DIDE, 
you have to get familiar with the simple process I explained above to make your own builds.

## High level goals

### Project management
I need an IDE that can help me easily setup different kind of projects for D programming.
Among the most important types are:

- snippets
- dub-based projects
- good, old (GNU) make projects
- we will support DMD *only*, until version 1 is released. After that we will gradually add support 
  for [GDC](http://www.gdcproject.org/) and [LDC](https://github.com/ldc-developers/ldc). 
  Maybe even [SDC](https://github.com/snazzy-d/SDC) if it reaches stable state.

**snippets** kind of project is a project which contains many (potentially hundreds) D files such that each of them can be compiled into an executable target. Ability to use RDMD to "directly" compile and run these D files is essential, but I also plan to implement "regular" compile/run cycle for each snippet. This is a perfect kind of project when you are reading a [D book](https://wiki.dlang.org/Books) and you want to toy with
a code sample from the book.

**[DUB](https://github.com/dlang/dub)** is de-facto standard build and project-control software for D programming, so it is quite expected from an IDE project to provide as good DUB support as possible.

**[(GNU) Make](https://www.gnu.org/software/make/)** is an old love of mine (being in software engineering business for over 3 decades) and I feel like Make support is a must even for an IDE created in 2020+.

I am sure that at some point in the future either me, or the D community, would add support for meson, ninja, CMake, WAF, or some other build system(s).

### Code completion

I am quite happy with the what the [DCD](https://github.com/dlang-community/DCD) project provides in terms of completion, so DCD integration is one of the main goals.

### Debugging

And naturally, the [GDB/MI](https://ftp.gnu.org/old-gnu/Manuals/gdb/html_node/gdb_211.html) integration as well.

### Git integration

Considering that there is already a good binding to the Git library for the D programming language it should not be that difficult to integrate Git into DIDE.

### Other programming languages and file-types.

Support for Python and BASH is something I need myself, and will definitely implement it at some point. Probably BASH first, then Python. Python support will _have to_ include both [CPython](https://python.org) and [PyPy](https://pypy.org) .

Release plan(s)
---------------

Instead of writing everything here I decided to move entirely to the GitLab milestones - https://gitlab.com/dejan/dide/-/milestones .

Versioning
----------

If N in x.N.x is an even number, it should be a "more stable" release than the one with an odd number N.

I will start with 0.1.0 (N=1), which is considered unstable. The goal of 0.2.0 (N=2) would then be to get rid of as many bugs as possible from the previous version, and make a more stable release.

I will follow this pattern until we come up with something better.

For the time I will allocate **two months** for unstable release milestones, and **one month** for the
stable/bugfix milestones. It is always each to change, if we find a good reason for that...

Similar projects
----------------

Following projects are IDE implementations far more powerful than DIDE. I suggest you take a look at all of them and find one which suits you best. Also worth checking are Emacs and Vi extensions for D.

- [code-d](https://marketplace.visualstudio.com/items?itemName=webfreak.code-d) - plugin for VisualStudio Code. Used for development of DIDE.
- [Dexed](https://gitlab.com/basile.b/dexed) - a nice IDE for D which uses the same technology as the famous [Lazarus project](https://www.lazarus-ide.org/).
- [Dlang IDE](https://github.com/buggins/dlangide) inactive, but very good source of ideas if you want to start working on your own IDE.
- [IDEA plugin](https://github.com/intellij-dlanguage/intellij-dlanguage). Actively developed and worth having a look if you, like me, use IDEA every day.
  
